const mongoose=require('mongoose')
const UserSchema = require('./Schema/UserSchema')
const imageSchema = require('./Schema/ImageSchema')
const Comment = require('./Schema/LikeComment')

const user=new mongoose.model('user',UserSchema)
const image=new mongoose.model('image',imageSchema)
const CommentPost=new mongoose.model('Comment',Comment)


module.exports={
    user,
    image,
    CommentPost
}