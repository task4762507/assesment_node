
const jwt =require('jsonwebtoken')
const { JWT_SECRET } = require('../config')
const { user } = require('../db')


async function authMiddleWare(req,res,next){
    const jwtToken=req.headers.authorization

    const token=jwtToken.split(' ')[1]

    const decode=jwt.verify(token,JWT_SECRET)

    if(decode){
      req.userId=decode.userId
            
        const jwtId=decode.jti
        
        const exist=await user.findOne({jti:jwtId})
        if(exist){
            next()
        }else{
            return res.status(400).json({
                msg:"session expired"
            })
        }
    }else{
        return res.status(400).json({
            msg:"invalid token"
        })
    }
}

module.exports=authMiddleWare
