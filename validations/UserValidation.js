const joi =require('joi')

const UserValid=joi.object({
    firstname:joi.string().required(),
    lastname:joi.string().required(),
    email:joi.string().email(),
    password:joi.string().required().min(8).max(16)
})

module.exports={
    UserValid
}