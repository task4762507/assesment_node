const joi=require('joi')

const loginValid=joi.object({
    email:joi.string().email(),
    password:joi.string().required().min(8).max(16)
})

module.exports={
    loginValid
}