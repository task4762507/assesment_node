const multer = require("multer");
const fs = require("fs");
const { image } = require("../db");
const path = require("path");
const mongodb=require('mongodb')


// uploading the post
module.exports.fileUpload = async (req, res) => {
  var obj = {
    id: req.userId,
    img: {
      data: path.join(__dirname, ".././uploads", req.file.filename),
    },
  };

  await image.create(obj);

  return res.status(200).json({
    msg: "upload",
    url:obj.img.data
  });
};

// deleting the post
module.exports.deleteUpload=async(req,res)=>{
    const id=req.query.id



    await image.deleteOne({_id:id})

    return  res.status(200).json({
        msg:"image deleted"
    })

}

// applying aggregation to get comment on the post
module.exports.getComments=async(req,res)=>{

    try{
        const id=req.query.id
        const db=await image.findOne({_id:id})
        console.log(db)
        if(!db){
            return res.status(400).json({
                msg:"image does not exist"
            })
        }
        const objectId=new mongodb.ObjectId(id)
        console.log("hh")
        const response =await image.aggregate([
            {
                    $match:{
                        _id:objectId
                    }
            },
            
            {
                $lookup:{
                    from:'comments',
                    localField:'id',
                    foreignField:'imageId',
                    as:"NewArray"
                }
                
            },
            {
                $unwind:"$NewArray"
            },
            {
                $project:{
                    _id:0,
                    comment:"$NewArray.comment",
                    img:1
                }
            }
        ])
    
        return res.status(200).json({
            response
        })
    }catch(error){
        return res.status(400).json({
            msg:"bad request"
        })
    }
    
}