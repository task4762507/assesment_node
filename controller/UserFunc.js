const { UserValid } = require("../validations/UserValidation")
const bcrypt=require('bcrypt')
const jwt=require('jsonwebtoken')
const { user } = require("../db")
const { loginValid } = require("../validations/LoginValidations")
const saltRounds=10
const { v4: uuidv4 } = require('uuid');
const { JWT_SECRET } = require("../config")


module.exports.CreateUser=async(req,res)=>{

   try{

    const validate = await UserValid.validateAsync(req.body)
         
    const firstname=req.body.firstname
    const lastname=req.body.lastname
    const email=req.body.email
    const password=req.body.password
    const exist=await user.findOne({email:email})

    if(exist){
        res.status(400).json({
            msg:"User already exist"
        })
    }

    const hashPassword= await bcrypt.hash(password,saltRounds)
    console.log(hashPassword)
    await user.create({
        firstname:firstname,
        lastname:lastname,
        email:email,
        password:hashPassword
    })
    res.status(200).json({
        msg:"User Created"
    })




   }catch(error){
    return res.status(400).json({
        msg:error.message
    })
   }
}

module.exports.LoginUser=async(req,res)=>{
    try{
            const valid=await loginValid.validateAsync(req.body)
            const email=req.body.email
            const password=req.body.password

            const exist= await user.findOne({email:email})
            console.log(exist)

            if(!exist){
                return res.status(400).json({
                    msg:"user not created yet , create a user first"
                })
            }
            const pw=exist.password
            console.log(password)
            console.log(pw)
            // const hashPassword=await bcrypt.compare(password,pw)
            const hashPassword= await bcrypt.compare(password,pw)
            console.log(hashPassword)
         
            if(hashPassword){
                const userId=exist._id
                const jti=uuidv4()
                 
                await user.updateOne({_id:userId},{jti:jti})

                const token=jwt.sign({userId:userId,jti,role:"user"},JWT_SECRET)

                res.status(200).json({
                    msg:"Logged in",
                    token
                })
            }else{
                res.status(400).json({
                    msg:"incorrect password"
                })
            }
        }catch(error){
        return res.status(400).json({
            msg:error.message
        })
    }
}

