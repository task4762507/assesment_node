const express=require('express')
const multer=require('multer')
const { CreateUser, LoginUser } = require('../controller/UserFunc')
const authMiddleWare = require('../middleware/AuthMiddleWare')
const { fileUpload, deleteUpload, getComments } = require('../controller/FileUpload')
var path = require('path');
const { commentPost } = require('../controller/PostDetails')

// storing file with extension
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
      },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname))
    }
});
const upload = multer({ storage: storage })

const router=express.Router()


// User creation Routes

router.post('/userCreate',CreateUser)

//login routes

router.post('/login',LoginUser)

//file upload routes

router.post('/upload',authMiddleWare,upload.single('image'),fileUpload)

//delete file routes

router.delete('/delete',authMiddleWare,deleteUpload)

//comment on the post
router.post('/comment',commentPost)

//get comment on the post

router.get('/getPost',getComments)


module.exports=router