const express=require('express')
const app=express()
const MainRouter=require('./routes/UserRoutes')
const { mongoose_connect } = require('./config')

app.use(express.json())

app.use('/api/v1',MainRouter)

app.listen(3000,()=>{
    mongoose_connect
})