const mongoose=require('mongoose')

const imageSchema=new mongoose.Schema({
    id:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user'
    },
    img:{
        data:String,
        ContentType:String

    }
    
    
})

module.exports=imageSchema
