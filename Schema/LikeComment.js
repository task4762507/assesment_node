const { number } = require('joi')
const mongoose=require('mongoose')

const Comment=new mongoose.Schema({
    imageId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"image"
    },
   
    comment:{
        type:String
    }
})

module.exports=Comment