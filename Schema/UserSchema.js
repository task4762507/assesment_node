const mongoose=require('mongoose')

const UserSchema=new mongoose.Schema({
    firstname:{
        type:String,
        lowercase:true
    },
    lastname:{
        type:String,
        lowercase:true
    },
    email:{
        type:String,
        lowercase:true
    },
    password:{
        type:String,
       
    },
    jti:{
        type:String
    }
})

module.exports=UserSchema